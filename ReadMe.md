## Warner Music Culture Shifters 

This is a NodeJs Application developed by Midas Touch Technologies for the purpose of Collecting User submission data for the Culture Shifters Website.

Before You run this Application you need to install the latest version of [NodeJS](https://nodejs.org/en/) on your system.

### How to run this application

1. Open your terminal (or CMD if you are running this on a windows server)
2. CD into the directory of this folder
3. `cd src`
4. `npm install`
5. `node app.js`

This app is set to use an environment variable of the PORT it should run to. If a PORT environment variable is not provided it will run on port 3022

