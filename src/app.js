var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
const { sendMail } = require("./lib/mailer");
const { notifyMidasTouch } = require("./lib/notify");

var app = express();

app.use(bodyParser.json())

app.use(cors())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", '*');
  res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
  next();
});

const port = process.env.PORT || 3022;

app.post("/receive-submission", (req, res) => {
  sendMail(req.body)
  .then(() => {
    res.sendStatus(200)
  })
  .catch(err => {
    res.sendStatus(500)
    notifyMidasTouch(err)
  })
});

app.listen(port, () => {
  console.log(`WMA app listening on port ${port}`);
});
