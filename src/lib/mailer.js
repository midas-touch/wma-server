"use strict";
const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function sendMail(data) {
    console.log("sending mail")
    console.log(data)
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();
  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "midastouchemailservice@gmail.com",
      pass: "owhlcvwcmmaaycqv",
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: "midastouchemailservice@gmail.com", // sender address
    to: "cultureshifters@warnermusic.com", // list of receivers
    subject: "New Submission", // Subject line
    text: "Please find below details of new submission", // plain text body
    html: `
    <html>
        <head>
        <style>
        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }
        
        table caption {
            font-size: 1.5em;
            margin: .5em 0 .75em;
        }
        
        table tr {
            background-color: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }
        
        table th,
        table td {
            padding: .625em;
            text-align: left;
        }
        
        table th {
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
            color: #fff;
                background-color: #212529;
                border-color: #32383e;
        }
        
        @media screen and (max-width: 600px) {
            table {
            border: 0;
            }
        
            table caption {
            font-size: 1.3em;
            }
            
            table thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
            }
            
            table tr {
            border-bottom: 3px solid #ddd;
            display: block;
            margin-bottom: .625em;
            }
            
            table td {
            border-bottom: 1px solid #ddd;
            display: block;
            font-size: .8em;
            text-align: right;
            }
            
            
            table td::before {
            /*
            * aria-label has no advantage, it won't be read inside a table
            content: attr(aria-label);
            */
            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
            }
            
            table td:last-child {
            border-bottom: 0;
            }
        }
        </style>
        </head>
        <body>
        <div>
      <br/>
      <table>
        <thead>
            <tr>
                <th> Detail </th>
                <th> Data </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td> Institution </td>
                <td> ${data?.institution} </td>
            </tr>
            <tr>
                <td> Course </td>
                <td> ${data?.courseEnrolled} </td>
            </tr>
            <tr>
                <td> Name </td>
                <td> ${data?.name} </td>
            </tr>
            <tr>
                <td> Surname </td>
                <td> ${data?.surname} </td>
            </tr>
            <tr>
                <td> Age </td>
                <td> ${data?.age} </td>
            </tr>
            <tr>
                <td> Gender </td>
                <td> ${data?.gender} </td>
            </tr>
            <tr>
                <td> Race </td>
                <td> ${data?.race} </td>
            </tr>
            <tr>
                <td> Curriculum Vitae </td>
                <td><a href="${data?.cv}" target="_blank"> ${data?.cv ?? "Not provided"}</a> </td>
            </tr>
            <tr>
                <td> Instagram Link </td>
                <td> <a href="${data?.instagramUrl}" target="_blank" >${data?.instagramUrl ?? "Not provided"}</a> </td>
            </tr>
            <tr>
                <td> Twitter Link </td>
                <td> <a href="${data?.twitterUrl}" target="_blank" >${data?.twitterUrl ?? "Not provided"}</a> </td>
            </tr>
            <tr>
                <td> TikTok Link </td>
                <td> <a href="${data?.tiktokUrl}" target="_blank" >${data?.titktokUrl ?? "Not provided"}</a> </td>
            </tr>
            <tr>
                <td> Youtube Link </td>
                <td> <a href="${data?.youtubeUrl}" target="_blank" >${data?.youtubeUrl ?? "Not provided"}</a> </td>
            </tr>
            <tr>
                <td> Video Submission Link </td>
                <td> <a href="${data?.videoLink}" target="_blank" >${data?.videoLink ?? "Not provided"}</a> </td>
            </tr>
            <tr>
                <td>Motivation </td>
                <td> ${data?.motivation} </td>
            </tr>
        </tbody>
        </table>
      </div>
    </body>
    </html>

    
      `, 
  });
}

module.exports = {
  sendMail,
};
