const axios = require("axios");

const midasTouchHook = "https://hooks.slack.com/services/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"

function notifyMidasTouch (error) {
    const messageBody = {
        "username": "WMA Error notifier", 
        "text": "```"+error.stack+"```", 
        "icon_emoji": ":x:", 
        "attachments":[]
    };
    return axios.post(midasTouchHook, messageBody)
}

module.exports = {
    notifyMidasTouch,
}
  
  
